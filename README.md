## Compiled in slackware64 15.0 GCC 10.3.0 GLIBC 2.33
~~~

* ATK         Version: 2.36.0
* Cairo       Version: 1.16.0
* Gdk-Pixbuf  Version: 2.42.6
* Glib2       Version: 2.68.1
* GTK+2       Version: 2.24.33
* Harfbuzz    Version: 2.8.1
* Libffi      Version: 3.3.0
* Pango       Version: 1.48.4
* Pixman      Version: 0.40.0

~~~

## Build and Clean
~~~

* How to build "sh 3p_Buildme"
* build-cmd.sh will clean up before rebuilding.
* So if your build is done move your packages out.
* How to clean after done. " git clean -dfxx"

~~~
