#!/bin/bash

# This build-cmd.sh file is derived from one contributed by "Nicky D."
# <sl.nicky.ml@googlemail.com>. Many thanks!

# turn on verbose debugging output for logs.
set -x
# make errors fatal
set -e
# complain about unset env variables
set -u

if [ -z "$AUTOBUILD" ] ; then 
    exit 1
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

# run build commands from root checkout directory
stage="$(pwd)"
cd "$(dirname "$0")"
top="$(pwd)"

"$autobuild" source_environment > "$stage/variables_setup.sh" || exit 1
. "$stage/variables_setup.sh"


# The builds we perform clutter the source tree as well as the build and stage
# directories. This clean_subdir function is to start fresh for reproducible
# results.
if [ -d "$top/.git" ]
then # Mercurial repository
     function clean_subdir {
        # Not only does the build generate new files, it also modifies ones
        # that came in the upstream source tarball. 
        git checkout master "$1"
        # purge is a Mercurial extension bundled with the base package, but it
        # must be explicitly enabled. It removes all files not already
        # committed to the repository: all those flagged with '?' in 'hg
        # status' output. Don't fail the build if it's not enabled.
        git clean -dfxx "$1" || echo "hg purge not enabled, continuing"
     }
#elif [ -d "$top/.git" ] ...
else # We don't know what source control this uses! Do nothing.
     function clean_subdir {
        echo -n .
     }
fi
for f in "${top}"/source/*.tar.gz; do tar xf "$f"; done 
# These are all the upstream packages we build.
packages=(libffi glib atk pixman cairo pango gdk-pixbuf gtk+ harfbuzz)

# NB Nicky (Dasmijn): It's best to revert and purge those, the configure and
# builds steps will mess with those files. So it's best to reset this to a
# clean state.
#if [ -z "${LL_NO_PREBUILD_PURGE:+1}" ]
#then for pkg in "${packages[@]}"
#     do clean_subdir "$pkg"
#     done
#     rm -rf build
#fi

# If this package contained a single library, we'd fish out the library
# version from the upstream source and use that. As it is, however, it
# packages eight (!) different upstream libraries, each with its own distinct
# 3-part version number. Attempting to concatenate them into a single
# composite package version number would be horrific, meaningless to a human
# reader and impossibly obnoxious to type (e.g. to manipulate the tarball
# file). So we invent a version number instead.
echo "2.1" > "${stage}/VERSION.txt"
# That said, we feel duty bound to report them SOMEHOW.
function get_version {
    local var="$1"
    local dir="$2"
    local ver="$(sed -n -E "s/PACKAGE_VERSION='(.*)'/\1/p" "$top/$dir/configure")"
    eval $var=$ver
    echo "$var = $ver"
}



get_version ATK_VERSION        atk
get_version CAIRO_VERSION      cairo
get_version GDK_PIXBUF_VERSION gdk-pixbuf
get_version GLIB_VERSION       glib
get_version GTK_VERSION        gtk+
get_version HARFBUZZ_VERSION   harbuzz
get_version LIBFFI_VERSION     libffi
get_version PANGO_VERSION      pango
get_version PIXMAN_VERSION     pixman

function mesonbuildglib {
    pushd "$1"
    shift
    # The rest of the command line is possible additional configure switches.
    rm -rf meson-build
    mkdir meson-build
    cd meson-build
    local opts="-m$AUTOBUILD_ADDRSIZE $LL_BUILD_RELEASE"
    local compopts="$opts -static-libstdc++ -Wno-unused-local-typedefs"
    CFLAGS="$compopts" \
    CXXFLAGS="$compopts" \
    LDFLAGS="$opts ${LDFLAGS:-}" \
    meson setup \
    --prefix="${build}" \
    --libdir="${build}"/lib64 \
    --buildtype=release \
    -Dinstalled_tests=true \
    --default-library=both \
    -Dgtk_doc=false \
    .. 
    ninja -j8
    ninja install
    rm -rf "${build}"/lib64/*.la
    popd
}
function mesonbuild2 {
    pushd "$1"
    shift
    # The rest of the command line is possible additional configure switches.
    rm -rf meson-build
    mkdir meson-build
    cd meson-build
    local opts="-m$AUTOBUILD_ADDRSIZE $LL_BUILD_RELEASE"
    local compopts="$opts -static-libstdc++ -Wno-unused-local-typedefs"
    CFLAGS="$compopts" \
    CXXFLAGS="$compopts" \
    LDFLAGS="$opts ${LDFLAGS:-}" \
    meson setup \
    --prefix="${build}" \
    --libdir="${build}"/lib64 \
    --buildtype=release \
    --default-library=both \
    .. 
    ninja -j8
    ninja install
    rm -rf "${build}"/lib64/*.la
    popd
}
function mesonbuildhfbuz {
    pushd "$1"
    shift
    # The rest of the command line is possible additional configure switches.
    rm -rf meson-build
    mkdir meson-build
    cd meson-build
    local opts="-m$AUTOBUILD_ADDRSIZE $LL_BUILD_RELEASE"
    local compopts="$opts -static-libstdc++ -Wno-unused-local-typedefs"
    CFLAGS="$compopts" \
    CXXFLAGS="$compopts" \
    LDFLAGS="$opts ${LDFLAGS:-}" \
    meson setup \
    --prefix="${build}" \
    --libdir="${build}"/lib64 \
    --buildtype=release \
    --default-library=both \
    -Dtests=disabled \
    -Dbenchmark=disabled \
    -Ddocs=disabled \
    .. 
    ninja -j8
    ninja install
    rm -rf "${build}"/lib64/*.la
    popd
}
function mesonbuildpango {
    pushd "$1"
    shift
    # The rest of the command line is possible additional configure switches.
    rm -rf meson-build
    mkdir meson-build
    cd meson-build
    local opts="-m$AUTOBUILD_ADDRSIZE $LL_BUILD_RELEASE"
    local compopts="$opts -static-libstdc++ -Wno-unused-local-typedefs"
    CFLAGS="$compopts" \
    CXXFLAGS="$compopts" \
    LDFLAGS="$opts ${LDFLAGS:-}" \
    meson setup \
    --prefix="${build}" \
    --libdir="${build}"/lib64 \
    --buildtype=release \
    --default-library=both \
    -Dgtk_doc=false \
    .. 
    ninja -j8
    ninja install
    rm -rf "${build}"/lib64/*.la
    popd
}
function mesonbuildpixbf {
    pushd "$1"
    shift
    # The rest of the command line is possible additional configure switches.
    rm -rf meson-build
    mkdir meson-build
    cd meson-build
    local opts="-m$AUTOBUILD_ADDRSIZE $LL_BUILD_RELEASE"
    local compopts="$opts -static-libstdc++ -Wno-unused-local-typedefs"
    CFLAGS="$compopts" \
    CXXFLAGS="$compopts" \
    LDFLAGS="$opts ${LDFLAGS:-}" \
    meson setup \
    --prefix="${build}" \
    --libdir="${build}"/lib64 \
    --buildtype=release \
    --default-library=both \
    -Dinstalled_tests=false \
    -Dx11=true \
    -Dgir=true \
    -Dgtk_doc=false \
    .. 
    ninja -j8
    ninja install
    rm -rf "${build}"/lib64/*.la
    popd
}
function buildPkg {
    pushd "$1"
    shift
    # The rest of the command line is possible additional configure switches.
    local opts="-m$AUTOBUILD_ADDRSIZE $LL_BUILD_RELEASE"
    local compopts="$opts  -Wno-unused-local-typedefs"
    ./configure --prefix="${build}" --libdir="${build}"/lib64 --enable-static=yes "$@" 
    make -j8
    make install
    rm -rf "${build}"/lib64/*.la
    popd
}
function buildPkggtk {
    pushd "$1"
    shift
    # The rest of the command line is possible additional configure switches.
    local opts="-m$AUTOBUILD_ADDRSIZE $LL_BUILD_RELEASE"
    local compopts="$opts  -Wno-unused-local-typedefs"
    ./configure --prefix="${build}" --libdir="${build}"/lib64 --enable-static=yes --with-xinput=yes --enable-xkb --enable-introspection --enable-static=yes  "$@" 
    make -j8
    make install
    rm -rf "${build}"/lib64/*.la
    popd
}
function autoreconfPkg {
    pushd "$1"
    # Insert real comment explaining arcane autoconf incantation.
    # Thanks again to NickyD for providing it.
##  libtoolize
    # "force & install links in source tree"
    autoreconf -f -i
    popd
}


case "${AUTOBUILD_PLATFORM}" in 
    windows*|darwin*)
        echo "Not used on $AUTOBUILD_PLATFORM"
        ;;

    linux*)

        # got tripped up by lack of these packages on a dev Linux system
        which pkg-config > /dev/null \
        || fail "You must install pkg-config: sudo apt-get install (pkgconf or pkg-config)"
        which aclocal > /dev/null \
        || fail "You must install automake: sudo apt-get install automake"

        # NickyD's strategy is to build in a separate 'build' directory tree,
        # then migrate only the relevant files into our 'stage' directory.
        build="${top}/build"
        pkgconfig="${build}"/pkgconfig
        mkdir -p "$pkgconfig"

        # Generate a suitable libpng.pc file containing the correct version
        # number. The version number is found in autobuild.xml. Make autobuild
        # report it, then parse the Python literal and extract just what we
        # care about.
        # Drakeo read below
        # Only need it at build time. /usr/bin/pkg-config looks everywhere  not sure how efective this is.
        # Also /usr/bin/libpng-config linked to /usr/bin/libpng16-config will always point to the installed version. 
        # The call -I${stage}/packages/include/libpng16 will help.?
        # All distros did this durring migration building libpng12 and libpng16 together and this caused libpng16 to
        # link to libng12 as when building GTK+ and all programs that call on /usr/bin/libpng-config.
        # This is why you must have the same version installed. per the libpng12 and libpng16 issues.
        # This happens when migrating from libng12 to libpng16. Will be the same in the future upcoming 1.6.X to 1.7.x ? 1.8.x
        # You really should on older systems running both purge libpng and install same version you want to build against.
        # This caused me to build install libpng Autobuild version. Then build this in console.
        # You can then revert your system to make usable.
        # As of right now autobuild uses "1.6.37" latest stable release. http://www.libpng.org/pub/png/libpng.html
        LIBPNG_VERSION="$("$autobuild" installables print | python -c "
import sys, ast
print ast.literal_eval(sys.stdin.read())['libpng']['version']")"
        # Now emit libpng.pc
        cat > "$pkgconfig/libpng.pc" <<EOF
Name: libpng
Description: Loads and saves PNG files
Version: $LIBPNG_VERSION
Libs: -lpng16 -lz
Libs.private: -lm -lz -lm
Cflags: 
EOF

        export LD_LIBRARY_PATH="${build}/lib64"
        export CPPFLAGS="-I${stage}/packages/include -I${stage}/packages/include/zlib -I${stage}/packages/include/pcre -I${stage}/packages/include/fontconfig -I${stage}/packages/include/libpng16 ${CPPFLAGS:-}"
        # NickyD says of the '$ORIGIN' reference in LDFLAGS below:
        #
        # This is not a shell variable :) It's to embed $ORIGIN:$ORIGIN/../lib
        # into the elf images rpath section.
        #
        # The rpath section is there to embed hints where to search for
        # dependent libraries, the whole idea is to get rid of having to mess
        # with LD_LIBRARY_PATH in the viewer script so the viewer finds all so
        # files it needs in lib/ $ORIGIN means -> look in bin/ (where the real
        # exe is) $ORIGIN/../lib means -> look in bin/../lib/ otherwise.
        #
        # Here's a link where I am sure they explain this better than I can do:
        # https://enchildfone.wordpress.com/2010/03/23/a-description-of-rpath-origin-ld_library_path-and-portable-linux-binaries/
        #
        # I'd generally advise to use rpath, but then you have to go the full
        # mile and build the viewer and some other libs with the same
        # settings.
        export LDFLAGS="-L${stage}/packages/lib/release -Wl,-rpath,"'\$$ORIGIN:\$$ORIGIN/../lib64'
        export PKG_CONFIG_PATH="${build}/lib64/pkgconfig:${pkgconfig}"
        export PATH="$PATH:${build}/bin"

        mesonbuildhfbuz "harfbuzz"
        buildPkg "libffi"
        # glib also builds subdirectories gmodule, gthread, gobject, gio,
        # inotify, libcharset -- if at some point any of these become
        # autobuild packages in their own right (a la pcre), suppress local
        # builds of those as well and add the relevant dependencies to
        # autobuild.xml.
        mesonbuildglib "glib" 
        mesonbuild2 "atk" 

        # For pixman run autoreconf to make autotools happy
        autoreconfPkg "pixman"
        buildPkg "pixman" --without-demos
        autoreconfPkg "cairo"
        buildPkg "cairo"
        mesonbuildpango  "pango"  
        mesonbuildpixbf "gdk-pixbuf"
        autoreconfPkg "gtk+"
        buildPkggtk "gtk+" --with-xinput=yes --enable-xkb  --enable-introspection 

        mkdir -p "${stage}/lib/release/"

        cp -R "${build}/include" "${stage}/"
        cp -R "${build}/lib64"/* "${stage}/lib/release/"
        cp "${stage}/lib/release/glib-2.0/include"/* "${stage}/include/glib-2.0/"
        cp -R "${stage}/include/gdk-pixbuf-2.0/gdk-pixbuf" "${stage}/include/"

        cp  "${top}/gtk+/gdk/gdkconfig.h" "${stage}/include/gtk-2.0/"

        rm -rf "${stage}/lib/release"/*png*
        rm -rf "${stage}/include"/*png*
        rm -rf "${stage}/lib/release"/*fontconfig*
        rm -rf "${stage}/include"/*fontconfig*

        # Remove the so/a archive depending on what shall be packaged up.
        # rm -rf "${stage}/lib/release"/*.a
        # rm -rf "${stage}/lib/release"/*.so*
        ;;

    *)
        fail "Unknown AUTOBUILD_PLATFORM='${AUTOBUILD_PLATFORM}'"
        ;;
esac

mkdir -p "$stage/LICENSES"
# Composite license file: identify which section pertains to which library.
function sep {
    python -c "print '\n' + r''' $1 '''.center(72, '=')"
}

# Pull the contents of the license file from each component library.
cat <(sep "atk $ATK_VERSION")               "$top/atk/COPYING" \
    <(sep "cairo $CAIRO_VERSION")           "$top/cairo/COPYING-LGPL-2.1" \
    <(sep "gdk-pixbuf $GDK_PIXBUF_VERSION") "$top/gdk-pixbuf/COPYING" \
    <(sep "glib $GLIB_VERSION")             "$top/glib/COPYING" \
    <(sep "gtk+ $GTK_VERSION")              "$top/gtk+/COPYING" \
    <(sep "harfbuzz $HARFBUZZ_VERSION")     "$top/harfbuzz/COPYING" \
    <(sep "libffi $LIBFFI_VERSION")         "$top/libffi/LICENSE" \
    <(sep "pango $PANGO_VERSION")           "$top/pango/COPYING" \
    <(sep "pixman $PIXMAN_VERSION")         "$top/pixman/COPYING" \
    > "$stage/LICENSES/gtk-atk-pango-glib.txt"

